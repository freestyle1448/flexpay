import QtQuick 2.0
import Sailfish.Silica 1.0
import "../models"

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    SilicaListView {
        id: listView
        model: mainLogic
        width: page.width
        height: 1000 //page.height - buttonPay.height - Theme.paddingLarge - Theme.paddingLarge
        spacing: Theme.paddingLarge
        header: PageHeader {
            title: qsTr("Shopping cart")
        }
        PullDownMenu {
                         MenuItem {
                             text: qsTr("About")
                             onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"));
                         }
                         MenuItem {
                             text: qsTr("Clear all")
                             onClicked: mainLogic.cleanCart();
                         }
                         MenuLabel { text: qsTr("Settings") }
                     }
        delegate: BackgroundItem {
            id: listItemOne
            height: rowq.height + Theme.paddingLarge + Theme.paddingLarge
            Row {
                id: rowq
                spacing: Theme.paddingMedium
                IconButton {

                    id: adwr
                    icon.source: "image://theme/icon-l-image"
                    // onClicked: counter++
                }
                Label {
                    id: itemLabel
                    text: model.name
                }
            }
            Row {
                id: rowa
                anchors.top: rowq.bottom
                IconButton {
                    id: remove
                    icon.source: "image://theme/icon-m-remove"
                    onClicked: {if(model.cntProd > 0){model.cntProd--; mainLogic.getSum();} else {"0"}}
                    }
                Label {
                    text: model.cntProd
                    color: Theme.secondaryHighlightColor
                    font.pixelSize: Theme.fontSizeExtraLarge
                    }
                IconButton {
                    id: add
                    icon.source: "image://theme/icon-m-add"
                    onClicked: {model.cntProd++; mainLogic.getSum(); console.info(mainLogic.summ)}
                }
            }
            Label {
                anchors.top: rowq.bottom
                anchors.right: parent.right
                anchors.rightMargin: Theme.paddingMedium
                text: model.price * model.cntProd
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeExtraLarge
                }

        }
        VerticalScrollDecorator {}
    }
    Button {
        id: buttonPay
        anchors.bottom: page.bottom
        anchors.bottomMargin: Theme.horizontalPageMargin
        anchors.horizontalCenter: parent.horizontalCenter
        width: page.width - Theme.horizontalPageMargin
        text: qsTr("Pay %1 rubles").arg(mainLogic.summ);
        onClicked: {kassir.pay(mainLogic.summ); pageStack.push(Qt.resolvedUrl("PaymentPage.qml"));}
    }
}
