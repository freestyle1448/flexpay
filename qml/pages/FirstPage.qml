import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        // Tell SilicaFlickable the height of its content.
        contentHeight: column.height

        // Place our content in a Column.  The PageHeader is always placed at the top
        // of the page, followed by our content.
        Column {
            id: column

            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                title: "flexPay"
            }
            Rectangle{
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                height: page.width - Theme.horizontalPageMargin
                color: Theme.rgba(Theme.highlightBackgroundColor, Theme.highlightBackgroundOpacity)
            }
            Label {
                x: Theme.horizontalPageMargin
                text: qsTr("chocolate")
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeLarge
            }
            Label {
                x: Theme.horizontalPageMargin
                text: qsTr("price 30")
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeMedium
            }
            Button {
                text: "generate bar code"
                onClicked: mainLogic.getRandomeBarCode();
            }

        }
    }
    Button {
        anchors.bottom: page.bottom
        anchors.bottomMargin: Theme.horizontalPageMargin
        anchors.horizontalCenter: parent.horizontalCenter
        width: page.width - Theme.horizontalPageMargin
        text: "Go to shopping cart"
        onClicked: pageStack.push(Qt.resolvedUrl("SecondPage.qml"));
    }
}
