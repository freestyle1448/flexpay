import QtQuick 2.0
import Sailfish.Silica 1.0
import QtWebEngine 1.0
Page {
    id: page

    WebEngineView {
        id: webView
        anchors.fill: parent
        url: paymentUrl
        onUrlChanged: console.log(url)
    }
}
