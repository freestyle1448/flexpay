import QtQuick 2.0
import Sailfish.Silica 1.0
import "pages"
import "models"
import Kassir 1.0

ApplicationWindow
{
    property MainLogic mainLogic: MainLogic { }
    property YaKassir kassir: YaKassir {
        onReadyURLChanged: {
            console.log(readyURL);
            paymentUrl = "https://money.yandex.ru/api-pages/v2/payment-confirm/" + readyURL;
        }
    }
    property url paymentUrl

    initialPage: Component { FirstPage { } }
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: defaultAllowedOrientations
}
