import QtQuick 2.0

ListModel {
    id: model
    property var summ;
    function findBarCode(barcode) {
        var product = new function(){
            this.name = "Product" + barcode;
            this.price = barcode;
            this.cntProd = 1;
        }
        return product;
    }
    function addProduct(barcode) {
        var product = findBarCode(barcode);
        append({
            name: product.name,
            price: product.price,
            cntProd: product.cntProd,
        });
    }
    function getSum() {
        var summer = 0;
        for(var i = 0; i < model.rowCount(); i++){
            summer = summer + (get(i).price * get(i).cntProd);
        }
        summ = summer;

    }

    function getRandomeBarCode() {
        var rand = 1000 - 0.5 + Math.random() * (9999 - 1000 + 1)
            rand = Math.round(rand);
            //console.info(rand);
        addProduct(rand);
        console.info(model.rowCount());
        getSum();
        console.info(model.summ);
        return rand;
    }
    function cleanCart() {
        clear();
        summ = 0;
    }

    Component.onCompleted: {
        //var notes = Database.readAll();
        //Utils.fillListModel(model, notes);
    }
}
