#include "yakassir.h"
#include <random>
#include <QDebug>
#include <QUrlQuery>
#include <QJsonDocument>
#include <QJsonObject>

std::default_random_engine generator;
std::uniform_int_distribution<int> distribution(111,999);

YaKassir::YaKassir(QObject *parent) : QObject(parent) {

    connect( &m_manager, SIGNAL( finished( QNetworkReply* ) ), SLOT( onFinished( QNetworkReply* ) ) );

}


YaKassir::~YaKassir(){}
    QString YaKassir::getUUID(void){
        QString strUUID = "3520fb39-89be-43a0-1413-95619e464" + (QString)distribution(generator);
        qInfo() << strUUID;
        return strUUID;
        }
    void YaKassir::pay(double amount) {
        QString str = QObject::tr("{\"amount\": {\"value\": \"%1\",\"currency\": \"RUB\"},\"confirmation\": {\"type\": \"redirect\",\"return_url\": \"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQKp1UzQJ48Fg_EkdpOb562gvV-LAZV_v-qXMyS0Y89uvl-KRL-\"},\"capture\": true,\"description\": \"Заказ №1\"}").arg(amount);
        QByteArray jsonString = str.toLocal8Bit();

        QNetworkRequest request(QUrl("https://payment.yandex.net/api/v3/payments/"));
        request.setHeader(QNetworkRequest::ContentTypeHeader,
            "application/json");

        QString concatenated = "624243:test_ILuU0pMKC_wVQe7uMZsVizXu6B26voHs8Wl0yTGic3g";
        QByteArray data = concatenated.toLocal8Bit().toBase64();
        QString headerData = "Basic " + data;

        request.setRawHeader("Authorization", headerData.toLocal8Bit());
        request.setRawHeader("Idempotence-Key", QString(YaKassir::getUUID()).toLocal8Bit());
        m_manager.post(request, jsonString);
    }

    void YaKassir::onFinished( QNetworkReply* reply ) {
        if( reply->error() == QNetworkReply::NoError ) {
            m_data = QString::fromUtf8( reply->readAll() );
            //qInfo() << m_data;
            qInfo() << "Ready!" ;
            QStringList strList = m_data.split("\"\n  },\n  \"created_at\"");
            QString m_readyURL = strList.value(strList.length() - 2);
            while(!m_readyURL.startsWith("epl?orderId")){
                m_readyURL.remove(0, 1);
            }
            qInfo() << m_readyURL;
            emit readyURLChanged(m_readyURL);

        } else {
            qInfo() << reply->errorString();
        }

        reply->deleteLater();
    }
