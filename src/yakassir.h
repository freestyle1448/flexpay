#ifndef YAKASSIR_H
#define YAKASSIR_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>

class YaKassir : public QObject
{
    Q_OBJECT
private:
    QNetworkAccessManager m_manager;
    QString m_data;

    QString m_readyURL;

public:
   YaKassir(QObject *parent = nullptr);
   ~YaKassir();
    Q_INVOKABLE QString getUUID(void);
    Q_INVOKABLE void pay(double amount);
   Q_PROPERTY(QString readyURL READ readyURL WRITE setReadyURL NOTIFY readyURLChanged)
   Q_PROPERTY(QString data READ data WRITE setData NOTIFY dataChanged)
   QString data() const
   {
       return m_data;
   }

   QString readyURL() const
   {
       return m_readyURL;
   }

signals:

   void dataChanged(QString data);

   void readyURLChanged(QString readyURL);

private slots:
   void onFinished( QNetworkReply* reply );
public slots:
   void setData(QString data)
   {
       if (m_data == data)
           return;
       m_data = data;
       emit dataChanged(m_data);
   }
   void setReadyURL(QString readyURL)
   {
       if (m_readyURL == readyURL)
           return;

       m_readyURL = readyURL;
       emit readyURLChanged(m_readyURL);
   }
};

#endif // YAKASSIR_H
